package rf.androidovshchik.imageslider

import android.app.FragmentManager
import android.support.v13.app.FragmentStatePagerAdapter

class SlidersAdapter(fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager) {

    private var fragments: MutableList<SliderFragment> = ArrayList()

    init {
        fragments.add(SliderFragment.newInstance(R.layout.fragment_image, R.drawable.bear))
        fragments.add(SliderFragment.newInstance(R.layout.fragment_image, R.drawable.bonobo))
        fragments.add(SliderFragment.newInstance(R.layout.fragment_image, R.drawable.eagle))
        fragments.add(SliderFragment.newInstance(R.layout.fragment_image, R.drawable.horse))
        fragments.add(SliderFragment.newInstance(R.layout.fragment_web, 0))
    }

    override fun getItem(position: Int): SliderFragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return ""
    }
}