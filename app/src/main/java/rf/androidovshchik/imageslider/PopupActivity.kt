package rf.androidovshchik.imageslider

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebView

class PopupActivity : Activity() {

    @SuppressWarnings("all")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_popup)
        val web = findViewById<WebView>(R.id.web)
        web.webViewClient = WebClient()
        web.settings.javaScriptEnabled = true
        val html = "<style>" +
                "body {" +
                "  background: url('" + intent.getStringExtra("image") + "') no-repeat center center fixed;" +
                "  -webkit-background-size: cover;" +
                "  background-size: cover;" +
                "}" +
                "</style>" +
                "<body>" +
                "<a href=\"" + intent.getStringExtra("url") + "\"" +
                " style=\"display: inline-block;height: 100%;width: 100%;\"></a>" +
                "</body>"
        Log.d("html", html)
        web.loadData(html, "text/html", "UTF-8")
    }

    fun close(view : View) {
        startActivity(Intent(applicationContext, SecondActivity::class.java))
        finish()
    }
}