package rf.androidovshchik.imageslider

import android.app.Activity
import android.os.Bundle
import android.util.Log

class SecondActivity : Activity() {

    lateinit var viewPager: CustomViewPager

    lateinit var adapter: SlidersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        viewPager = findViewById(R.id.pager)
        adapter = SlidersAdapter(fragmentManager)
        viewPager.adapter = adapter
    }

    @SuppressWarnings("all")
    fun stopViewPager() {
        viewPager.enableSwipe = false
        KnockKnock(object: KnockKnock.Listener {
            override fun onResponse(response: Response) {
                if (!isFinishing) {
                    Log.d("SecondActivity response", response.toString())
                    if (response.enabled) {
                        adapter.getItem(adapter.count - 1).web?.webViewClient = WebClient()
                        adapter.getItem(adapter.count - 1).web?.settings?.javaScriptEnabled = true
                        adapter.getItem(adapter.count - 1).web?.loadUrl(response.url)
                    } else {
                        viewPager.enableSwipe = true
                        viewPager.currentItem = 0
                    }
                }
            }
        }).execute(getString(R.string.url_script_web))
    }
}
