package rf.androidovshchik.imageslider

import android.webkit.WebView
import android.webkit.WebViewClient

class WebClient: WebViewClient() {

    @Suppress("OverridingDeprecatedMember")
    override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
        if (url != null) {
            view.loadUrl(url)
        }
        return true
    }
}
