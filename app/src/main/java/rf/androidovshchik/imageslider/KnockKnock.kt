package rf.androidovshchik.imageslider

import android.os.AsyncTask
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class KnockKnock(private val listener: Listener): AsyncTask<String, Void, String>() {

    interface Listener {

        fun onResponse(response: Response)
    }

    override fun doInBackground(vararg strings: String): String? {
        val url: URL
        val urlConnection: HttpURLConnection
        var response: String? = null
        try {
            url = URL(strings[0])
            urlConnection = url.openConnection() as HttpURLConnection
            val responseCode = urlConnection.responseCode
            if (responseCode == HttpURLConnection.HTTP_OK) {
                response = readStream(urlConnection.inputStream)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return response
    }

    private fun readStream(`in`: InputStream): String {
        var reader: BufferedReader? = null
        val response = StringBuilder()
        try {
            reader = BufferedReader(InputStreamReader(`in`))
            var line: String?
            do {
                line = reader.readLine()
                if (line == null) {
                    break
                }
                response.append(line)
            } while (true)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        return response.toString()
    }

    override fun onPostExecute(response: String?) {
        super.onPostExecute(response)
        if (response != null) {
            try {
                val jsonObject = JSONObject(response)
                val responseObject = Response()
                responseObject.enabled = jsonObject.getBoolean("enabled")
                responseObject.url = jsonObject.getString("url")
                if (jsonObject.has("image")) {
                    responseObject.image = jsonObject.getString("image")
                }
                listener.onResponse(responseObject)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}