package rf.androidovshchik.imageslider

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.ImageView

class SliderFragment: Fragment() {

    var layout: Int = 0
    var image: Int = 0
    var web: WebView? = null

    companion object {
        fun newInstance(@LayoutRes layout: Int, @DrawableRes image: Int): SliderFragment {
            val fragment = SliderFragment()
            fragment.layout = layout
            fragment.image = image
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(getApplicationContext()).inflate(layout, null)
        if (image != 0) {
            view.findViewById<ImageView>(R.id.image).setImageResource(image)
        } else {
            web = view.findViewById(R.id.web)
        }
        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && web != null) {
            (activity as SecondActivity).stopViewPager()
        }
    }

    private fun getApplicationContext(): Context {
        return activity.applicationContext
    }
}