package rf.androidovshchik.imageslider

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class CustomViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    var enableSwipe: Boolean = true

    @SuppressWarnings("all")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (this.enableSwipe) {
            super.onTouchEvent(event)
        } else false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (this.enableSwipe) {
            super.onInterceptTouchEvent(event)
        } else false
    }
}
