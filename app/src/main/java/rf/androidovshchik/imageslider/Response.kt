package rf.androidovshchik.imageslider

class Response {

    var enabled: Boolean = false

    var image: String? = null

    var url: String? = null

    override fun toString(): String {
        return "Response(enabled=$enabled, image=$image, url=$url)"
    }
}
