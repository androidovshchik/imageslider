package rf.androidovshchik.imageslider

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log

class FirstActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        KnockKnock(object: KnockKnock.Listener {
            override fun onResponse(response: Response) {
                if (!isFinishing) {
                    Log.d("FirstActivity response", response.toString())
                    if (response.enabled) {
                        val intent = Intent(applicationContext, PopupActivity::class.java)
                        intent.putExtra("image", response.image)
                        intent.putExtra("url", response.url)
                        startActivity(Intent(intent))
                    } else {
                        startActivity(Intent(applicationContext, SecondActivity::class.java))
                    }
                    finish()
                }
            }
        }).execute(getString(R.string.url_script))
    }
}
